import java.util.Scanner;

public class ApplianceStore {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		
		for (int i = 0; i < toasters.length; i++){
			Toaster toaster = new Toaster();
			toasters[i] = toaster;
			toasters[i].height = scan.nextDouble();
			toasters[i].color = scan.next();
			toasters[i].buttons = scan.nextInt();
		}
		
		System.out.println();
		System.out.println(toasters[3].height);
		System.out.println(toasters[3].color);
		System.out.println(toasters[3].buttons);
		System.out.println();
		toasters[0].printHeight();
		toasters[0].printColor();
	}
}